# k8s Tech test

Trying a automated deployment of a Wordpress server on my own!

## Step:

- 1) Use a "Essai gratuit" subscription on Azure
- 2) Create a resource group under that subscription (eg. k8s-test)
- 3) Create a vnet (10.0.0.0/8) and a subnet (10.240.0.0/16) for enforcing network policy rules on the cluster : az network vnet create --resource-group k8s-test --name vnet-k8s --address-prefixes 10.0.0.0/8 --subnet-name subnet-k8s --subnet-prefix 10.240.0.0/16
- 4) Import cluster informations and create a service principal for gitlab to local env variables: source local_env.sh
- 5) Create k8s cluster:
    az aks create \
        --name tech-test-cluster \
        --resource-group k8s-test \
        --ssh-key-value /Users/yan/Documents/kubernetes-tests/k8s-tech-test/wordpress-cluster/ssh-key-wordpress-cluster.pub \
        --node-count 1 \
        --service-principal $SP_ID \
        --client-secret $SP_PASSWD \
        --dns-service-ip 10.0.0.10 \
        --docker-bridge-address 172.17.0.1/16 \
        --network-plugin azure \
        --network-policy azure \
        --service-cidr 10.0.0.0/16 \
        --vnet-subnet-id $SUBNET_ID \
        --output table
- 6) Update my k8s client coniguration file:
    az aks get-credentials \
        --name tech-test-cluster \
        --resource-group k8s-test \
        --output table
